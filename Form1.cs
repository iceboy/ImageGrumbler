﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ImageGrumbler {
    public partial class Form1 : Form {
        [DllImport("kernel32.dll", EntryPoint = "CopyMemory", SetLastError = false)]
        public static extern void CopyMemory(IntPtr dest, IntPtr src, uint count);

        private Bitmap m_orig;

        public Form1(string filename) {
            m_orig = (Bitmap)Bitmap.FromFile(filename);
            InitializeComponent();
        }

        private Bitmap GetResizedImage(int cx) {
            int pixels = m_orig.Width * m_orig.Height;
            int cy = (pixels + (cx - 1)) / cx;
            BitmapData bd = m_orig.LockBits(new Rectangle(Point.Empty, m_orig.Size),
                                            ImageLockMode.ReadOnly,
                                            PixelFormat.Format32bppArgb);
            Debug.Assert(bd.Width * 4 == bd.Stride);
            try {
                Bitmap b = new Bitmap(cx, cy, PixelFormat.Format32bppArgb);
                BitmapData bd0 = b.LockBits(new Rectangle(Point.Empty, b.Size),
                                            ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
                Debug.Assert(bd0.Width * 4 == bd0.Stride);
                try {
                    CopyMemory(bd0.Scan0, bd.Scan0, (uint)pixels * 4);
                } finally {
                    b.UnlockBits(bd0);
                }
                return b;
            } finally {
                m_orig.UnlockBits(bd);
            }
        }

        private void Form1_LoadOrResize(object sender, EventArgs e) {
            this.BackgroundImage = GetResizedImage(this.ClientSize.Width);
        }
    }
}
